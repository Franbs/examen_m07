<?php
include('Class/UploadClass.php');
define("UPLOAD_FIELD", "doc");
define("TITLE_FIELD", "title");
define("SUBJECT_FIELD", "subject");
define('TITLE_ERROR', "Please write a title");
define('SUBJECT_ERROR', "Please select a subject");


class UploadError extends Exception
{
}
// Check if the form was submitted
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES[UPLOAD_FIELD])) {
    //check if title and subject is in the form data
    if (empty($_POST[TITLE_FIELD])) {
        header('Location: index.php?upload=error&msg=' . urlencode(TITLE_ERROR));
        return;
    }

    if (empty($_POST[SUBJECT_FIELD])) {
        header('Location: index.php?upload=error&msg=' . urlencode(SUBJECT_ERROR));
        return;
    }

    $file = $_FILES[UPLOAD_FIELD];
    $title = $_POST[TITLE_FIELD];
    $subject = $_POST[SUBJECT_FIELD];
    $upload = new Upload($file,$title);

    //If File is uploaded correcty, we added to our file
    if ($upload->getFile() != null)
        $upload->addUploadToFile($subject);
    // Any error redirect with error message
    if ($upload->getError() != null)
        header('Location: index.php?upload=error&msg=' . urlencode($upload->getError()));
    else header("Location: index.php?upload=success");
}
